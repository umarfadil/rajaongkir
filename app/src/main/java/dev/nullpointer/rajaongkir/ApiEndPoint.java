package dev.nullpointer.rajaongkir;

/**
 * Created by umarfadil on 9/18/17.
 */

public class ApiEndPoint {
    public static String BaseUrl = "https://api.rajaongkir.com/starter/";
    public static String Province = "province";
    public static String City = "city";
    public static String Cost = "cost";
    public static String ApiKey = "c6ff8937dda6e2c38e2f28bec13089d0";
}
