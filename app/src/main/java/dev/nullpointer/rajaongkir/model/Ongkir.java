package dev.nullpointer.rajaongkir.model;

/**
 * Created by umarfadil on 9/18/17.
 */

public class Ongkir {
    public long province_id;
    public String province;
    public String result;
    public long code;
    public long city_id;
    public String city_name;
}
